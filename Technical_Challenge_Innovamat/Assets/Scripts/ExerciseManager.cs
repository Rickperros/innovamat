﻿using UnityEngine;

public class ExerciseManager : MonoBehaviour
{
    [Header("Configuration")]

    [SerializeField]
    private Exercise m_exerciseObject;
    [SerializeField]
    private ExerciseData m_exerciseDataObject;
    [SerializeField]
    private ExerciseCanvas m_canvasObject;

    private IExercise m_exercise;
    private IExerciseData m_exerciseData;
    private IExerciseCanvas m_canvas;
    private SExerciseInfo m_currentExerciseInfo;
    private EInputState m_inputState = EInputState.IGNORED;
    private int m_succesCounter;
    private int m_failureCounter;
    void Start()
    {
        m_exercise = m_exerciseObject as IExercise;
        m_exerciseData = m_exerciseDataObject as IExerciseData;
        m_canvas = m_canvasObject.GetComponent<IExerciseCanvas>();
        m_canvas.SetUp(this);
        m_succesCounter = 0;
        m_failureCounter = 0;
        InitExercise();
        GetExerciseInfo();
    }

    private void InitExercise()
    {
        m_exercise.Clean();
        m_exercise.SetUp(m_exerciseData, this);
    }
    private void GetExerciseInfo()
    {
        if(m_exercise.RetrieveExerciseInfo(ref m_currentExerciseInfo))
        {
            m_canvas.DisplayExercise(m_currentExerciseInfo);
        }
        else
        {
            //if info can't be retrieved initialize exercise again
            InitExercise();
            GetExerciseInfo();
        }
    }

    public void ChangeInputState(EInputState newInputState)
    {
        m_inputState = newInputState;
    }

    public bool IsInputAllowed()
    {
        return m_inputState == EInputState.ALLOWED;
    }
    public bool CheckAnswer(int answer)
    {
        return answer == m_currentExerciseInfo.m_correctAnswer;
    }

    public void EndExercise(bool success)
    {
        if(success)
            m_succesCounter++;
        else
            m_failureCounter++;

        GetExerciseInfo();
    }

    public int GetSuccesResults()
    {
        return m_succesCounter;
    }

    public int GetFailureResulst()
    {
        return m_failureCounter;
    }
}
