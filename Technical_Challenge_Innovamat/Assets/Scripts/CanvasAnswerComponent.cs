﻿using UnityEngine;

public class CanvasAnswerComponent : MonoBehaviour
{
    private int m_index;
    private ExerciseCanvas m_owner;
    
    public void SetUp(int index, ExerciseCanvas owner)
    {
        m_index = index;
        m_owner = owner;
    }
    public void ClickReceived()
    {
        if(m_owner.IsInputAllowed())
        {
            m_owner.CheckAnswer(m_index);
        }
    }  
}
