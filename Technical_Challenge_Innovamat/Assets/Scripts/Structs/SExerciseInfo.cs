﻿public struct SExerciseInfo
{
    public string m_question;
    public string[] m_answers;
    public int m_correctAnswer; 
}
