﻿using UnityEngine;

[System.Serializable]
public class Exercise : ScriptableObject, IExercise
{
    protected ExerciseManager m_manager;
    protected IExerciseData m_data;
    public virtual bool Clean(){return false;}
    public virtual bool SetUp(IExerciseData data, ExerciseManager manager)
    {
        m_data = data;
        m_manager = manager;
        return true;
    }
    public virtual bool RetrieveExerciseInfo(ref SExerciseInfo exercise){return false;}
}
