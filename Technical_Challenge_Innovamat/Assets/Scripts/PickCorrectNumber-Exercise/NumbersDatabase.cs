﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NumbersDataBase", menuName = "ScriptableObjects/ExerciseData/NumbersDataBase", order = 1)]
public class NumbersDatabase : ExerciseData
{
    [SerializeField]
    private List<SNumber> m_numbers;

    public override void RetrieveData(params object[] list)
    {
        if(list[0] is List<SNumber>)
        {
            List<SNumber> l_data = list[0] as List<SNumber>;
            foreach(SNumber l_num in m_numbers)
            {
                l_data.Add(l_num);
            }
        }
    }
}