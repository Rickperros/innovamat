﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Animation))]
public class PickNumberCanvas : ExerciseCanvas
{
    [Header("Configuration")]
     [SerializeField]
    private float m_questionDisplayTime = 2.0f;
     [SerializeField]
    private string m_SuccesLiteral = "Aciertos: ";
     [SerializeField]
    private string m_FailureLiteral = "Fallos: ";

    [Header("Objects References")]
     [SerializeField]
    private GameObject m_AnswerPrefab;
     [SerializeField]
    private GameObject m_AnswersParent;
     [SerializeField]
    private Text m_QuestionText;
     [SerializeField]
    private Text m_FailureText;
     [SerializeField]
    private Text m_SuccesText;

    [Header("Animations")]
    [SerializeField]
    private AnimationClip m_DisplayQuestionAnim;
    [SerializeField]
     private AnimationClip m_HideQuestionAnim;
    [SerializeField]
    private AnimationClip m_DisplayAnswerAnim;
    [SerializeField]
    private AnimationClip m_HideAnswersAnim;

    private Animation m_AnimationComp;
    private int m_currentTry = 0;
    private List<RectTransform> m_answers;
    private bool m_succes = false;

    void Awake()
    {
        m_AnimationComp = GetComponent<Animation>();
        m_answers = new List<RectTransform>();
    }
    public override void SetUp(ExerciseManager manager)
    {
        base.SetUp(manager);
        SetScoreTextValue();
    }
    public override void DisplayExercise(SExerciseInfo info)
    {
        base.DisplayExercise(info);
        DisplayQuestion();
    }

    private void DisplayQuestion()
    {
        m_QuestionText.text = m_exerciseInfo.m_question;
        m_AnimationComp.clip = m_DisplayQuestionAnim;
        m_AnimationComp.Play();
    }

     public void InvokeHideQuestion()
    {
        Invoke("HideQuestion", m_questionDisplayTime);
    }

    public void HideQuestion()
    {
        m_AnimationComp.clip = m_HideQuestionAnim;
        m_AnimationComp.Play();
    }

    public void DisplayAnswers()
    {
        if(m_answers.Count > 0)
        {
            foreach(RectTransform l_transform in m_answers)
            {
                Destroy(l_transform.gameObject);
            }
        }
        m_answers = new List<RectTransform>();

        //Set up buttons info
        for(int i = 0; i < m_exerciseInfo.m_answers.Length; i++)
        {
            Button l_button = Instantiate(m_AnswerPrefab, transform.position, transform.rotation).GetComponent<Button>();
            l_button.GetComponentInChildren<Text>().text = m_exerciseInfo.m_answers[i];
            l_button.GetComponent<CanvasAnswerComponent>().SetUp(i, this);
            l_button.transform.SetParent(m_AnswersParent.transform);
            m_answers.Add(l_button.GetComponent<RectTransform>());
        }

        //Set up buttons position
        float l_offset = 200.0f;

        for(int i = 0; i < m_answers.Count; i++)
        {
            //Make sure scale is 1
            m_answers[i].localScale = Vector3.one;
            m_answers[i].localPosition = Vector3.right * (l_offset * i) + Vector3.up * m_answers[i].position.y + Vector3.forward * m_answers[i].position.z;
        }

        PlayShowAnswersAnim();
    }

    public void PlayShowAnswersAnim()
    {
        m_AnimationComp.clip = m_DisplayAnswerAnim;
        m_AnimationComp.Play();
    }

    public void AllowInput()
    {
        m_manager.ChangeInputState(EInputState.ALLOWED);
    }
    public override bool CheckAnswer(int AnswerIndex)
    {
        m_manager.ChangeInputState(EInputState.IGNORED);
        m_succes = false;

        if(!m_manager.CheckAnswer(AnswerIndex))
        {
            m_currentTry++;
            DisplayError(AnswerIndex);
            
            if(m_currentTry != 1)
            {
                DisplaySucces();
            }
            return m_succes;
        }

        DisplaySucces();
        m_succes = true;
        return m_succes;
    }
    public override void DisplayError(int AnswerIndex)
    {
        m_answers[AnswerIndex].GetComponent<Image>().color = Color.red;
        Invoke("PingPongHideAnswers", 0.5f);
    }

    public override void DisplaySucces()
    {
        m_answers[m_exerciseInfo.m_correctAnswer].GetComponent<Image>().color = Color.green;
        Invoke("HideAnswersAndEndExercise", 0.5f);
    }

    public void PingPongHideAnswers()
    {
        PlayHideAnswersAnim();
        Invoke("PlayShowAnswersAnim",m_AnimationComp.clip.length);
    }
    public void HideAnswersAndEndExercise()
    {
        PlayHideAnswersAnim();
        Invoke("EndExercise",m_AnimationComp.clip.length);
    }
    private void PlayHideAnswersAnim()
    {
        foreach(RectTransform l_answer in m_answers)
        {
            l_answer.GetComponent<Image>().color = Color.white;
        }
        m_AnimationComp.clip = m_HideAnswersAnim;
        m_AnimationComp.Play();
    }
    public void EndExercise()
    {
        m_manager.EndExercise(m_succes);
        SetScoreTextValue();
        m_currentTry = 0;
    }

    private void SetScoreTextValue()
    {
        m_SuccesText.text = m_SuccesLiteral+m_manager.GetSuccesResults().ToString();
        m_FailureText.text = m_FailureLiteral+m_manager.GetFailureResulst().ToString();
    }
}
