﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PickCorrectNumber", menuName = "ScriptableObjects/Exercises/PickCorrectNumber", order = 1)]
public class PickCorrectNumber : Exercise
{
    [SerializeField]
    private int m_amountOfAnswers = 3;
    [SerializeField]
    private string m_standardQuestion = "Pick Number: ";

    [HideInInspector]
    public List<SNumber> m_rawData;
    private List<SNumber> m_randomizedData;
    
    public override bool Clean()
    {
        m_rawData.Clear();
        m_randomizedData.Clear();
        return true;
    }
    public override bool SetUp(IExerciseData data, ExerciseManager manager)
    {
        base.SetUp(data, manager);
        m_data.RetrieveData(m_rawData);
        m_randomizedData = GetDataRandomized(m_rawData);
        return true;
    }

    private List<SNumber> GetDataRandomized(List<SNumber> rawData)
    {
        List<SNumber> l_randomizedData = new List<SNumber>();
        int rawDataSize = rawData.Count;

        while(l_randomizedData.Count < rawDataSize)
        {
            int l_index = Random.Range(0, rawData.Count);

            l_randomizedData.Add(rawData[l_index]);
            rawData.RemoveAt(l_index);
        }

        return l_randomizedData;
    }
    public override bool RetrieveExerciseInfo(ref SExerciseInfo exercise)
    {
        if(m_randomizedData.Count <= 0)
            return false;

        //Make sure raw data is full
        m_rawData.Clear();
        m_data.RetrieveData(m_rawData);

        //Make sure correct answer is in list
        List<SNumber> l_answers = new List<SNumber>();
        l_answers.Add(m_randomizedData[0]);
        m_rawData.Remove(m_randomizedData[0]);

        //Add Random answers
        for(int i = 1; i < m_amountOfAnswers; i++)
        {
             AddRandomAnswer(ref l_answers);
        }

        List<SNumber> l_randomizedAnswers = GetDataRandomized(l_answers);

        //Fill Exercise info
        exercise.m_question = m_standardQuestion + m_randomizedData[0].m_literal;
        exercise.m_correctAnswer = l_randomizedAnswers.FindIndex((x) => x.m_literal == m_randomizedData[0].m_literal);
        exercise.m_answers = new string[m_amountOfAnswers];

        for(int i = 0; i < m_amountOfAnswers; i++)
        {
            exercise.m_answers[i] = l_randomizedAnswers[i].m_number.ToString();
        }       
        
        //Quit this exercise from list
        m_randomizedData.RemoveAt(0);

        return true;
    }

    private void AddRandomAnswer(ref List<SNumber> l_answers)
    {
        int l_index = Random.Range(0, m_rawData.Count);
        l_answers.Add(m_rawData[l_index]);
        m_rawData.Remove(m_rawData[l_index]);
    }
}
