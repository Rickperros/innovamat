﻿using UnityEngine;

public class ExerciseData : ScriptableObject, IExerciseData
{
    public virtual void RetrieveData(params object[] list){}
}
