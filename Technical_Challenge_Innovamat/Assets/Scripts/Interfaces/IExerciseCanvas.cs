﻿public interface IExerciseCanvas
{
    void SetUp(ExerciseManager manager);
    void DisplayExercise(SExerciseInfo info);
    bool CheckAnswer(int answerIndex);
    void DisplayError(int AnswerIndex);
    void DisplaySucces();
}
