﻿public interface IExercise
{
    bool Clean();
    bool SetUp(IExerciseData data, ExerciseManager manager);
    bool RetrieveExerciseInfo(ref SExerciseInfo exercise);
}
