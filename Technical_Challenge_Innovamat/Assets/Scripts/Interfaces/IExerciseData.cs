﻿public interface IExerciseData
{
    void RetrieveData(params object[] list);
}
