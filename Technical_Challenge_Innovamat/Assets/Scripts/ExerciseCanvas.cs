﻿using UnityEngine;

public class ExerciseCanvas : MonoBehaviour, IExerciseCanvas
{
    protected ExerciseManager m_manager;
    protected SExerciseInfo m_exerciseInfo;
    protected int m_selectedAnswer;

    public virtual void SetUp(ExerciseManager manager)
    {
        m_manager = manager;
    }
    public virtual void DisplayExercise(SExerciseInfo info)
    {
        m_exerciseInfo = info;
    }
    public virtual bool CheckAnswer(int answerIndex)
    {
        return false;
    }
    public virtual void DisplayError(int AnswerIndex){}
    public virtual void DisplaySucces(){}

    public virtual bool IsInputAllowed()
    {
        return m_manager.IsInputAllowed();
    }
}
